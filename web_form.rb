require "selenium-webdriver"
require "rspec"

$first_name = 'Nurlaila'
$last_name = 'Qulbi'
$job_title = 'QA'
$date = '01/01/2021'
$expected_text = 'The form was successfully submitted!'

def submit_form(driver)
    driver.find_element(:id, 'first-name').send_keys $first_name
    driver.find_element(:id, 'last-name').send_keys $last_name
    driver.find_element(:id, 'job-title').send_keys $job_title
    driver.find_element(:id, 'radio-button-3').click
    driver.find_element(:id, 'checkbox-2').click
    sleep 2
    driver.find_element(:css, 'option[value="2"]').click
    driver.find_element(:id, 'datepicker').send_keys $date 
    driver.find_element(:id, 'datepicker').send_keys :return 
    sleep 2
    driver.find_element(:css, '.btn.btn-lg.btn-primary').click
end

def get_banner_text(driver)
    wait = Selenium::WebDriver::Wait.new(timeout:10)
    wait.until{driver.find_element(:class, 'alert').displayed?}
    the_actual = driver.find_element(:class, 'alert')
    banner = the_actual.text
end

describe "automating a form" do
    it "submits a form" do
        driver = Selenium::WebDriver.for:chrome
        driver.navigate.to "https://formy-project.herokuapp.com/form"
        driver.manage.window.maximize
        submit_form(driver)
        actual_banner = get_banner_text(driver)
        expect(actual_banner).to eql($expected_text)
    end
end

